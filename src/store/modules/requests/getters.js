export default {
    requests(state, _, _2, rootGetters) {
        const coachId = rootGetters.userId;
        const changed = [];
        for(let req of state.requests){
            if(req.coachId === coachId){
                changed.push(req);
            }
        }
        return changed;
    },
    hasRequests(_, getters) {
        return getters.requests && getters.requests.length > 0;
    }
};